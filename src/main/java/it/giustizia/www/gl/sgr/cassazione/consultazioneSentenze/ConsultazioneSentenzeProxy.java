package it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze;

public class ConsultazioneSentenzeProxy implements it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze {
  private String _endpoint = null;
  private it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze consultazioneSentenze = null;
  
  public ConsultazioneSentenzeProxy() {
    _initConsultazioneSentenzeProxy();
  }
  
  public ConsultazioneSentenzeProxy(String endpoint) {
    _endpoint = endpoint;
    _initConsultazioneSentenzeProxy();
  }
  
  private void _initConsultazioneSentenzeProxy() {
    try {
      consultazioneSentenze = (new it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.WsConsultazioneSentenzeLocator()).getConsultazioneSentenzeCiviliSOAPPort();
      if (consultazioneSentenze != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)consultazioneSentenze)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)consultazioneSentenze)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (consultazioneSentenze != null)
      ((javax.xml.rpc.Stub)consultazioneSentenze)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze getConsultazioneSentenze() {
    if (consultazioneSentenze == null)
      _initConsultazioneSentenzeProxy();
    return consultazioneSentenze;
  }
  
  public javax.activation.DataHandler downloadSentenza(java.lang.String annoSentenza, java.lang.String numeroSentenza) throws java.rmi.RemoteException{
    if (consultazioneSentenze == null)
      _initConsultazioneSentenzeProxy();
    return consultazioneSentenze.downloadSentenza(annoSentenza, numeroSentenza);
  }
  
  
}