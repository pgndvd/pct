/**
 * WsConsultazioneSentenze.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze;

public interface WsConsultazioneSentenze extends javax.xml.rpc.Service {
    public java.lang.String getConsultazioneSentenzeCiviliSOAPPortAddress();

    public it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze getConsultazioneSentenzeCiviliSOAPPort() throws javax.xml.rpc.ServiceException;

    public it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze getConsultazioneSentenzeCiviliSOAPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getConsultazioneSentenzePenaliSOAPPortAddress();

    public it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze getConsultazioneSentenzePenaliSOAPPort() throws javax.xml.rpc.ServiceException;

    public it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze getConsultazioneSentenzePenaliSOAPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
