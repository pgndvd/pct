/**
 * ConsultazioneSentenze.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze;

public interface ConsultazioneSentenze extends java.rmi.Remote {
    public javax.activation.DataHandler downloadSentenza(java.lang.String annoSentenza, java.lang.String numeroSentenza) throws java.rmi.RemoteException;
}
