/**
 * WsConsultazioneSentenzeLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze;

public class WsConsultazioneSentenzeLocator extends org.apache.axis.client.Service implements it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.WsConsultazioneSentenze {

    public WsConsultazioneSentenzeLocator() {
    }


    public WsConsultazioneSentenzeLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WsConsultazioneSentenzeLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ConsultazioneSentenzeCiviliSOAPPort
    private java.lang.String ConsultazioneSentenzeCiviliSOAPPort_address = "https://targetHost/targetPath";

    public java.lang.String getConsultazioneSentenzeCiviliSOAPPortAddress() {
        return ConsultazioneSentenzeCiviliSOAPPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ConsultazioneSentenzeCiviliSOAPPortWSDDServiceName = "ConsultazioneSentenzeCiviliSOAPPort";

    public java.lang.String getConsultazioneSentenzeCiviliSOAPPortWSDDServiceName() {
        return ConsultazioneSentenzeCiviliSOAPPortWSDDServiceName;
    }

    public void setConsultazioneSentenzeCiviliSOAPPortWSDDServiceName(java.lang.String name) {
        ConsultazioneSentenzeCiviliSOAPPortWSDDServiceName = name;
    }

    public it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze getConsultazioneSentenzeCiviliSOAPPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ConsultazioneSentenzeCiviliSOAPPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getConsultazioneSentenzeCiviliSOAPPort(endpoint);
    }

    public it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze getConsultazioneSentenzeCiviliSOAPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenzeCiviliSOAPBindingStub _stub = new it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenzeCiviliSOAPBindingStub(portAddress, this);
            _stub.setPortName(getConsultazioneSentenzeCiviliSOAPPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setConsultazioneSentenzeCiviliSOAPPortEndpointAddress(java.lang.String address) {
        ConsultazioneSentenzeCiviliSOAPPort_address = address;
    }


    // Use to get a proxy class for ConsultazioneSentenzePenaliSOAPPort
    private java.lang.String ConsultazioneSentenzePenaliSOAPPort_address = "https://targetHost/targetPath";

    public java.lang.String getConsultazioneSentenzePenaliSOAPPortAddress() {
        return ConsultazioneSentenzePenaliSOAPPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ConsultazioneSentenzePenaliSOAPPortWSDDServiceName = "ConsultazioneSentenzePenaliSOAPPort";

    public java.lang.String getConsultazioneSentenzePenaliSOAPPortWSDDServiceName() {
        return ConsultazioneSentenzePenaliSOAPPortWSDDServiceName;
    }

    public void setConsultazioneSentenzePenaliSOAPPortWSDDServiceName(java.lang.String name) {
        ConsultazioneSentenzePenaliSOAPPortWSDDServiceName = name;
    }

    public it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze getConsultazioneSentenzePenaliSOAPPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ConsultazioneSentenzePenaliSOAPPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getConsultazioneSentenzePenaliSOAPPort(endpoint);
    }

    public it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze getConsultazioneSentenzePenaliSOAPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenzePenaliSOAPBindingStub _stub = new it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenzePenaliSOAPBindingStub(portAddress, this);
            _stub.setPortName(getConsultazioneSentenzePenaliSOAPPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setConsultazioneSentenzePenaliSOAPPortEndpointAddress(java.lang.String address) {
        ConsultazioneSentenzePenaliSOAPPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze.class.isAssignableFrom(serviceEndpointInterface)) {
                it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenzeCiviliSOAPBindingStub _stub = new it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenzeCiviliSOAPBindingStub(new java.net.URL(ConsultazioneSentenzeCiviliSOAPPort_address), this);
                _stub.setPortName(getConsultazioneSentenzeCiviliSOAPPortWSDDServiceName());
                return _stub;
            }
            if (it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenze.class.isAssignableFrom(serviceEndpointInterface)) {
                it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenzePenaliSOAPBindingStub _stub = new it.giustizia.www.gl.sgr.cassazione.consultazioneSentenze.ConsultazioneSentenzePenaliSOAPBindingStub(new java.net.URL(ConsultazioneSentenzePenaliSOAPPort_address), this);
                _stub.setPortName(getConsultazioneSentenzePenaliSOAPPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ConsultazioneSentenzeCiviliSOAPPort".equals(inputPortName)) {
            return getConsultazioneSentenzeCiviliSOAPPort();
        }
        else if ("ConsultazioneSentenzePenaliSOAPPort".equals(inputPortName)) {
            return getConsultazioneSentenzePenaliSOAPPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.giustizia.it/gl/sgr/cassazione/consultazioneSentenze", "WsConsultazioneSentenze");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.giustizia.it/gl/sgr/cassazione/consultazioneSentenze", "ConsultazioneSentenzeCiviliSOAPPort"));
            ports.add(new javax.xml.namespace.QName("http://www.giustizia.it/gl/sgr/cassazione/consultazioneSentenze", "ConsultazioneSentenzePenaliSOAPPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ConsultazioneSentenzeCiviliSOAPPort".equals(portName)) {
            setConsultazioneSentenzeCiviliSOAPPortEndpointAddress(address);
        }
        else 
if ("ConsultazioneSentenzePenaliSOAPPort".equals(portName)) {
            setConsultazioneSentenzePenaliSOAPPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
