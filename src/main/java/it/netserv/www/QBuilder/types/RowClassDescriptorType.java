/**
 * RowClassDescriptorType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netserv.www.QBuilder.types;

public class RowClassDescriptorType  implements java.io.Serializable {
    private it.netserv.www.QBuilder.types.PropertyDescType[] property;

    private it.netserv.www.QBuilder.types.ClassRefType[] classRef;

    public RowClassDescriptorType() {
    }

    public RowClassDescriptorType(
           it.netserv.www.QBuilder.types.PropertyDescType[] property,
           it.netserv.www.QBuilder.types.ClassRefType[] classRef) {
           this.property = property;
           this.classRef = classRef;
    }


    /**
     * Gets the property value for this RowClassDescriptorType.
     * 
     * @return property
     */
    public it.netserv.www.QBuilder.types.PropertyDescType[] getProperty() {
        return property;
    }


    /**
     * Sets the property value for this RowClassDescriptorType.
     * 
     * @param property
     */
    public void setProperty(it.netserv.www.QBuilder.types.PropertyDescType[] property) {
        this.property = property;
    }

    public it.netserv.www.QBuilder.types.PropertyDescType getProperty(int i) {
        return this.property[i];
    }

    public void setProperty(int i, it.netserv.www.QBuilder.types.PropertyDescType _value) {
        this.property[i] = _value;
    }


    /**
     * Gets the classRef value for this RowClassDescriptorType.
     * 
     * @return classRef
     */
    public it.netserv.www.QBuilder.types.ClassRefType[] getClassRef() {
        return classRef;
    }


    /**
     * Sets the classRef value for this RowClassDescriptorType.
     * 
     * @param classRef
     */
    public void setClassRef(it.netserv.www.QBuilder.types.ClassRefType[] classRef) {
        this.classRef = classRef;
    }

    public it.netserv.www.QBuilder.types.ClassRefType getClassRef(int i) {
        return this.classRef[i];
    }

    public void setClassRef(int i, it.netserv.www.QBuilder.types.ClassRefType _value) {
        this.classRef[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RowClassDescriptorType)) return false;
        RowClassDescriptorType other = (RowClassDescriptorType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.property==null && other.getProperty()==null) || 
             (this.property!=null &&
              java.util.Arrays.equals(this.property, other.getProperty()))) &&
            ((this.classRef==null && other.getClassRef()==null) || 
             (this.classRef!=null &&
              java.util.Arrays.equals(this.classRef, other.getClassRef())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProperty() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProperty());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProperty(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getClassRef() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getClassRef());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getClassRef(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RowClassDescriptorType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.netserv.it/QBuilder/types", "rowClassDescriptorType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("property");
        elemField.setXmlName(new javax.xml.namespace.QName("", "property"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.netserv.it/QBuilder/types", "propertyDescType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classRef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "classRef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.netserv.it/QBuilder/types", "classRefType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
