/**
 * RowType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netserv.www.QBuilder.types;

public class RowType  implements java.io.Serializable {
    private it.netserv.www.QBuilder.types.NamedValueType[] property;

    private it.netserv.www.QBuilder.types.SrowType[][] subRows;

    private java.lang.String _class;  // attribute

    public RowType() {
    }

    public RowType(
           it.netserv.www.QBuilder.types.NamedValueType[] property,
           it.netserv.www.QBuilder.types.SrowType[][] subRows,
           java.lang.String _class) {
           this.property = property;
           this.subRows = subRows;
           this._class = _class;
    }


    /**
     * Gets the property value for this RowType.
     * 
     * @return property
     */
    public it.netserv.www.QBuilder.types.NamedValueType[] getProperty() {
        return property;
    }


    /**
     * Sets the property value for this RowType.
     * 
     * @param property
     */
    public void setProperty(it.netserv.www.QBuilder.types.NamedValueType[] property) {
        this.property = property;
    }

    public it.netserv.www.QBuilder.types.NamedValueType getProperty(int i) {
        return this.property[i];
    }

    public void setProperty(int i, it.netserv.www.QBuilder.types.NamedValueType _value) {
        this.property[i] = _value;
    }


    /**
     * Gets the subRows value for this RowType.
     * 
     * @return subRows
     */
    public it.netserv.www.QBuilder.types.SrowType[][] getSubRows() {
        return subRows;
    }


    /**
     * Sets the subRows value for this RowType.
     * 
     * @param subRows
     */
    public void setSubRows(it.netserv.www.QBuilder.types.SrowType[][] subRows) {
        this.subRows = subRows;
    }

    public it.netserv.www.QBuilder.types.SrowType[] getSubRows(int i) {
        return this.subRows[i];
    }

    public void setSubRows(int i, it.netserv.www.QBuilder.types.SrowType[] _value) {
        this.subRows[i] = _value;
    }


    /**
     * Gets the _class value for this RowType.
     * 
     * @return _class
     */
    public java.lang.String get_class() {
        return _class;
    }


    /**
     * Sets the _class value for this RowType.
     * 
     * @param _class
     */
    public void set_class(java.lang.String _class) {
        this._class = _class;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RowType)) return false;
        RowType other = (RowType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.property==null && other.getProperty()==null) || 
             (this.property!=null &&
              java.util.Arrays.equals(this.property, other.getProperty()))) &&
            ((this.subRows==null && other.getSubRows()==null) || 
             (this.subRows!=null &&
              java.util.Arrays.equals(this.subRows, other.getSubRows()))) &&
            ((this._class==null && other.get_class()==null) || 
             (this._class!=null &&
              this._class.equals(other.get_class())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProperty() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProperty());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProperty(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSubRows() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubRows());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubRows(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (get_class() != null) {
            _hashCode += get_class().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RowType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.netserv.it/QBuilder/types", "rowType"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("_class");
        attrField.setXmlName(new javax.xml.namespace.QName("", "class"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("property");
        elemField.setXmlName(new javax.xml.namespace.QName("", "property"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.netserv.it/QBuilder/types", "namedValueType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subRows");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subRows"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.netserv.it/QBuilder/types", "subRowsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
