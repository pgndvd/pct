/**
 * ServiceDescriptorType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netserv.www.QBuilder.types;

public class ServiceDescriptorType  implements java.io.Serializable {
    private it.netserv.www.QBuilder.types.PropertyDescType[] params;

    private it.netserv.www.QBuilder.types.ClassRefType[] rowClass;

    public ServiceDescriptorType() {
    }

    public ServiceDescriptorType(
           it.netserv.www.QBuilder.types.PropertyDescType[] params,
           it.netserv.www.QBuilder.types.ClassRefType[] rowClass) {
           this.params = params;
           this.rowClass = rowClass;
    }


    /**
     * Gets the params value for this ServiceDescriptorType.
     * 
     * @return params
     */
    public it.netserv.www.QBuilder.types.PropertyDescType[] getParams() {
        return params;
    }


    /**
     * Sets the params value for this ServiceDescriptorType.
     * 
     * @param params
     */
    public void setParams(it.netserv.www.QBuilder.types.PropertyDescType[] params) {
        this.params = params;
    }

    public it.netserv.www.QBuilder.types.PropertyDescType getParams(int i) {
        return this.params[i];
    }

    public void setParams(int i, it.netserv.www.QBuilder.types.PropertyDescType _value) {
        this.params[i] = _value;
    }


    /**
     * Gets the rowClass value for this ServiceDescriptorType.
     * 
     * @return rowClass
     */
    public it.netserv.www.QBuilder.types.ClassRefType[] getRowClass() {
        return rowClass;
    }


    /**
     * Sets the rowClass value for this ServiceDescriptorType.
     * 
     * @param rowClass
     */
    public void setRowClass(it.netserv.www.QBuilder.types.ClassRefType[] rowClass) {
        this.rowClass = rowClass;
    }

    public it.netserv.www.QBuilder.types.ClassRefType getRowClass(int i) {
        return this.rowClass[i];
    }

    public void setRowClass(int i, it.netserv.www.QBuilder.types.ClassRefType _value) {
        this.rowClass[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceDescriptorType)) return false;
        ServiceDescriptorType other = (ServiceDescriptorType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.params==null && other.getParams()==null) || 
             (this.params!=null &&
              java.util.Arrays.equals(this.params, other.getParams()))) &&
            ((this.rowClass==null && other.getRowClass()==null) || 
             (this.rowClass!=null &&
              java.util.Arrays.equals(this.rowClass, other.getRowClass())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getParams() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getParams());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getParams(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRowClass() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRowClass());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRowClass(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceDescriptorType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.netserv.it/QBuilder/types", "serviceDescriptorType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("params");
        elemField.setXmlName(new javax.xml.namespace.QName("", "params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.netserv.it/QBuilder/types", "propertyDescType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rowClass");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rowClass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.netserv.it/QBuilder/types", "classRefType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
