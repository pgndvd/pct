package it.netserv.www.QBuilder.sigpbe;

public class ServiziQueryBuilderProxy implements it.netserv.www.QBuilder.sigpbe.ServiziQueryBuilder {
  private String _endpoint = null;
  private it.netserv.www.QBuilder.sigpbe.ServiziQueryBuilder serviziQueryBuilder = null;
  
  public ServiziQueryBuilderProxy() {
    _initServiziQueryBuilderProxy();
  }
  
  public ServiziQueryBuilderProxy(String endpoint) {
    _endpoint = endpoint;
    _initServiziQueryBuilderProxy();
  }
  
  private void _initServiziQueryBuilderProxy() {
    try {
      serviziQueryBuilder = (new it.netserv.www.QBuilder.sigpbe.WSServiziQueryBuilderLocator()).getServiziQueryBuilderSOAPPort();
      if (serviziQueryBuilder != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)serviziQueryBuilder)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)serviziQueryBuilder)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (serviziQueryBuilder != null)
      ((javax.xml.rpc.Stub)serviziQueryBuilder)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.netserv.www.QBuilder.sigpbe.ServiziQueryBuilder getServiziQueryBuilder() {
    if (serviziQueryBuilder == null)
      _initServiziQueryBuilderProxy();
    return serviziQueryBuilder;
  }
  
  public it.netserv.www.QBuilder.types.RowType[] execute(java.lang.String name, it.netserv.www.QBuilder.types.NamedValueType[] valueSet, it.netserv.www.QBuilder.types.OrderByEntryType[] orderBy) throws java.rmi.RemoteException{
    if (serviziQueryBuilder == null)
      _initServiziQueryBuilderProxy();
    return serviziQueryBuilder.execute(name, valueSet, orderBy);
  }
  
  public it.netserv.www.QBuilder.types.RowClassDescriptorType getRowClassDescriptor(java.lang.String name) throws java.rmi.RemoteException{
    if (serviziQueryBuilder == null)
      _initServiziQueryBuilderProxy();
    return serviziQueryBuilder.getRowClassDescriptor(name);
  }
  
  public it.netserv.www.QBuilder.types.ServiceDescriptorType getServiceDescriptor(java.lang.String name) throws java.rmi.RemoteException{
    if (serviziQueryBuilder == null)
      _initServiziQueryBuilderProxy();
    return serviziQueryBuilder.getServiceDescriptor(name);
  }
  
  public java.lang.String[] getServiceNames() throws java.rmi.RemoteException{
    if (serviziQueryBuilder == null)
      _initServiziQueryBuilderProxy();
    return serviziQueryBuilder.getServiceNames();
  }
  
  
}