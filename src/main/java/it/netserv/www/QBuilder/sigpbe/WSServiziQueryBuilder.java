/**
 * WSServiziQueryBuilder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netserv.www.QBuilder.sigpbe;

public interface WSServiziQueryBuilder extends javax.xml.rpc.Service {
    public java.lang.String getServiziQueryBuilderSOAPPortAddress();

    public it.netserv.www.QBuilder.sigpbe.ServiziQueryBuilder getServiziQueryBuilderSOAPPort() throws javax.xml.rpc.ServiceException;

    public it.netserv.www.QBuilder.sigpbe.ServiziQueryBuilder getServiziQueryBuilderSOAPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
