/**
 * ServiziQueryBuilder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netserv.www.QBuilder.sigpbe;

public interface ServiziQueryBuilder extends java.rmi.Remote {
    public it.netserv.www.QBuilder.types.RowType[] execute(java.lang.String name, it.netserv.www.QBuilder.types.NamedValueType[] valueSet, it.netserv.www.QBuilder.types.OrderByEntryType[] orderBy) throws java.rmi.RemoteException;
    public it.netserv.www.QBuilder.types.RowClassDescriptorType getRowClassDescriptor(java.lang.String name) throws java.rmi.RemoteException;
    public it.netserv.www.QBuilder.types.ServiceDescriptorType getServiceDescriptor(java.lang.String name) throws java.rmi.RemoteException;
    public java.lang.String[] getServiceNames() throws java.rmi.RemoteException;
}
