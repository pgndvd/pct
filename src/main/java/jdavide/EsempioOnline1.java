//package jdavide;
//
//import java.net.URI;
//
///**
// * http://www.ncryptoki.com/Forums/Thread.aspx?pageid=9&t=67~1
// * 
// * @author davide
// *
// */
//public class EsempioOnline1 {
//	/// <summary>
//	///
//	/// </summary>
//	/// <param name="url"></param>
//	/// <returns></returns>
//	public static HttpWebRequest CreateWebRequest(String url) {
//
//		URI uri = new URI(url);
//
//		HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(uri);
//		webRequest.Headers.Add("SOAP:Action");
//		webRequest.Headers.Add("X-WASP-User", "psssrl52s19h703u");
//		webRequest.KeepAlive = true;
//		webRequest.ContentType = "text/xml;charset=\"utf-8\"";
//		webRequest.Accept = "text/xml";
//		webRequest.Method = "POST";
//		webRequest.AuthenticationLevel = AuthenticationLevel.MutualAuthRequired;
//
//		X509Certificate2 SmartCardCertificate = GetSmartCardCertificate();
//		if (SmartCardCertificate != null) {
//			webRequest.ClientCertificates.Add(SmartCardCertificate);
//		}
//
//		return webRequest;
//
//	}
//}
