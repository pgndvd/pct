//package jdavide;
//
//import java.net.URI;
//
///**
// * http://www.ncryptoki.com/Forums/Thread.aspx?pageid=9&t=67~1
// * 
// * @author davide
// *
// */
//public class EsempioOnline2 {
//	/// <summary>
//	/// GetServiceNames
//	/// </summary>
//	/// <param name="myUrn"></param>
//	/// <returns></returns>
//	private static XmlDocument CreateEnvelope()
//	{
//	   XmlDocument retVal = new XmlDocument();
//
//	String soapenv = String.Format(
//	@"<soap-env:Envelope xmlns:soap-env=""http://schemas.xmlsoap.org/soap/envelope/"">
//	<soap-env:Header>
//	<ws:invocationDomain
//	   xmlns:ws=""http://www.netserv.it/anag/security""
//	   soap-env:MustUnderstand=""true""
//	group=""jpwusers"" role=""JPW"" name=""JPW"">
//	</ws:invocationDomain>
//	</soap-env:Header>
//	<soap-env:Body>                           
//	<m:getServiceNames xmlns:m=""urn:CONS-SICC-BE"">
//	</m:getServiceNames>
//	</soap-env:Body>
//	</soap-env:Envelope>"
//	);
//
//	retVal.LoadXml(soapenv);
//	return retVal;
//	}
//
//	/// <summary>
//	///
//	/// </summary>
//	/// <param name="url"></param>
//	/// <returns></returns>
//	public static HttpWebRequest CreateWebRequest(string url) {
//
//		Uri uri = new Uri(url);
//
//		HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(uri);
//		webRequest.Proxy = null;
//		webRequest.Headers.Add("SOAP:Action");
//		webRequest.KeepAlive = true;
//		webRequest.Headers.Add("X-WASP-User", "psssrl52s19h703u");
//		webRequest.ContentType = "text/xml;charset=\"utf-8\"";
//		webRequest.Accept = "text/xml";
//		webRequest.Method = "POST";
//		webRequest.AuthenticationLevel = AuthenticationLevel.MutualAuthRequired;
//		if (certificate != null) {
//			webRequest.ClientCertificates.Add(certificate);
//		}
//
//		return webRequest;
//	}
//
//	/// <summary>
//	/// Funzione per accesso asincrono al PolisWeb
//	/// </summary>
//	/// <param name="url"></param>
//	/// <param name="envelope"></param>
//	/// <returns></returns>
//	public static XmlDocument ServiceCall(string url, XmlDocument envelope)
//	{
//	   XmlDocument resp = new XmlDocument();
//	try
//	{
//	if (NetworkInterface.GetIsNetworkAvailable())
//	{
//
//	if (certificate == null)
//	{
//	certificate = GetCertificateFromSmartCard();
//	}
//	   HttpWebRequest request = CreateWebRequest(url);
//
//	using (Stream stream = request.GetRequestStream())
//	{
//	   envelope.Save(stream);
//	}
//
//	IAsyncResult asyncResult = request.BeginGetResponse(null, null);
//
//	asyncResult.AsyncWaitHandle.WaitOne();
//
//	string soapResult = "";
//	using (WebResponse webResponse = request.EndGetResponse(asyncResult))
//	using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
//	{
//	  soapResult = rd.ReadToEnd();
//	}
//
//	resp.LoadXml(soapResult);
//	}
//	else
//	{
//	   MessageBox.Show("La connessione ad internet non è disponibile. \nPrima di riprovare assicurarsi che questo computer sia collegato alla rete.");
//	}
//
//	}
//	catch (WebException we)
//	{
//	if (we.Status == WebExceptionStatus.ProtocolError)
//	{
//	  WebResponse webResponse = we.Response;
//	  StreamReader sr = new StreamReader(webResponse.GetResponseStream());
//	string error = sr.ReadToEnd();
//	   MessageBox.Show(error);
//	}
//	else
//	{
//	   MessageBox.Show(we.ToString());
//	}
//
//	}
//	catch (Exception ex)
//	{
//	   MessageBox.Show(ex.Message);
//	}
//
//	return resp;
//	}
//}
